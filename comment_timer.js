(function ($) {
  /**
   * @file
   * Start the timer when the page is fully loaded.
   */
  var commentTimerDefined = false;
  Drupal.behaviors.comment_timer = {
    attach: function (context) {
      // Don't allow this function to run more than once.
      if (commentTimerDefined === false) {
        commentTimerDefined = true;

        var startTime = new Date().getTime();
        var paused = false;

        // Stores the counted time from the last start event.
        var currentElapsedSeconds = 0;
        // Stores the whole counted time.
        var elapsedSeconds = 0;

        // Add running class for the counter input field.
        $('#edit-comment-timer-counter').addClass('running');

        // Start/pause the clock.
        $('#edit-comment-timer-pause').click(function(event) {
          event.preventDefault();

          // If the clock is running, then pause it.
          if ($('#edit-comment-timer-counter').hasClass('running')) {
            // Change class on the input field to indicate that the counter is
            // paused.
            $('#edit-comment-timer-counter').removeClass('running').addClass('paused');
            $(this).val(Drupal.t('Run'));

            // Remember the currently elapsed seconds.
            elapsedSeconds = currentElapsedSeconds;
            paused = true;
          }
          // If the clock is paused, then start it.
          else {
            // Change class on the input field to indicate that the counter is
            // running.
            $('#edit-comment-timer-counter').removeClass('paused').addClass('running');
            $(this).val(Drupal.t('Pause'));

            // Get time from the input field as it could be changed and check
            // for the correct format.
            var pattern = /^([0-9]{2}):([0-5][0-9]):([0-5][0-9])$/;
            var result = pattern.exec($('#edit-comment-timer-counter').val());
            if (result !== null) {
              elapsedSeconds = parseInt(result[1]) * 3600 + parseInt(result[2]) * 60 + parseInt(result[3]);
            }

            startTime = new Date().getTime();
            paused = false;
          }
        });

        // Reset the clock, update the counter in the input field and update
        // the startTime with the current time.
        $('#edit-comment-timer-reset').click(function(event) {
          event.preventDefault();
          startTime = new Date().getTime();
          elapsedSeconds = 0;
          $('#edit-comment-timer-counter').val('00:00:00');
        });

        // The counter.
        (function counter() {
          // Calculate only the elapsed time only if it is not paused.
          if (!paused) {
            var time = new Date().getTime();

            // Store the currently counted seconds for later user and for
            // displaying it.
            // The current elapsed time is calculated as the following:
            // The counted elapsed seconds before (not counting while paused)
            // plus the current time minus the last start time.
            currentElapsedSeconds = elapsedSeconds + Math.floor((time - startTime) / 1000);

            // Get the seconds, minutes, hours from the timestamp to display it
            // in a human readable form.
            var seconds = currentElapsedSeconds % 60;
            var minutes = Math.floor(currentElapsedSeconds / 60) % 60;
            var hours = Math.floor(currentElapsedSeconds / 3600) % 100;

            // Update the counter input field with the currently counted time.
            $('#edit-comment-timer-counter').val((hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds));
          }

          window.setTimeout(counter, 1000);
        })();
      }
    }
  }
})(jQuery);
